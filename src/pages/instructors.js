import React from "react";
import { graphql } from "gatsby";

export default function Instructors({data}) {
  return (
    <div>
      <h1>Full Stack Citation Instructors</h1>
      <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Title</th>
                <th>Phone</th>
                <th>Office</th>
            </tr>
        </thead>
        <tbody>
            {data.allMongodbCpsc2650Instructors.edges.map(({ node }, index) => (
                <tr key={index}>
                    <td>{node.name}</td>
                    <td>{node.title}</td>
                    <td>{node.phone}</td>
                    <td>{node.office}</td>
                </tr>
            ))}
        </tbody>
      </table>
    </div>
  )
}

export const query = graphql`
query InstructorsQuery {
  allMongodbCpsc2650Instructors {
    edges {
      node {
        name
        office
        phone
        title
      }
    }
  }
}
`